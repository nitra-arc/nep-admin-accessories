# AccessoriesBundle

## Описание

данный бандл предназначен для:

* **создания коллекции аксессуаров на товары, категории и бренды**

## Подключение

* **composer.json**

```json
{
    ...   
    "require": {
        ...
        "nitra/e-commerce-admin-accessoriesbundle": "dev-master",
        ...
    }
    ...
}
```

* **app/AppKernel.php**

```php
<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    //...
    public function registerBundles()
    {
        //...
        $bundles = array(
            //...
            new Nitra\AccessoriesBundle\NitraAccessoriesBundle(),
            //...
        );
        //...
        return $bundles;
    }
    //...
}
```

* **routing.yml**

```yml
nitra_accessories:
    resource:   "@NitraAccessoriesBundle/Resources/config/routing.yml"
    prefix:     /{_locale}/
    defaults:
        _locale: ru
    requirements:
        _locale: en|ru
```

* **menu.yml**

```yml
 accessories:
     translateDomain: 'menu'
     route: Nitra_AccessoriesBundle_Accessorie_list
```