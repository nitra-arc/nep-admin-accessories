<?php

namespace Nitra\AccessoriesBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
use Nitra\ProductBundle\Document\Product;

/**
 * @ODM\Document
 */
class Accessorie
{
    /**
     * @var string Identifier
     * @ODM\Id
     */
    protected $id;

    /**
     * @var string Collection name
     * @ODM\String
     * @Gedmo\Translatable
     */
    protected $name;

    /**
     * @var \Nitra\ProductBundle\Document\Category Products category
     * @ODM\ReferenceOne(targetDocument="Nitra\ProductBundle\Document\Category")
     */
    protected $category;

    /**
     * @var \Nitra\ProductBundle\Document\Brand Products brand
     * @ODM\ReferenceOne(targetDocument="Nitra\ProductBundle\Document\Brand")
     */
    protected $brand;

    /**
     * @var Product[] Products
     * @ODM\ReferenceMany(targetDocument="Nitra\ProductBundle\Document\Product")
     */
    protected $accessories;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->accessories = new ArrayCollection();
    }
    
    /**
     * Get id
     * @return string $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set category
     * @param \Nitra\ProductBundle\Document\Category $category
     * @return self
     */
    public function setCategory($category)
    {
        $this->category = $category;
        return $this;
    }

    /**
     * Get category
     * @return \Nitra\ProductBundle\Document\Category $category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set brand
     * @param Nitra\ProductBundle\Document\Brand $brand
     * @return self
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;
        return $this;
    }

    /**
     * Get brand
     * @return \Nitra\ProductBundle\Document\Brand $brand
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * Add accessory
     * @param Product $accessory
     * @return self
     */
    public function addAccessory(Product $accessory)
    {
        $this->accessories[] = $accessory;
        return $this;
    }

    /**
     * Remove accessory
     * @param \Nitra\ProductBundle\Document\Product $accessory
     * @return self
     */
    public function removeAccessory(Product $accessory)
    {
        $this->accessories->removeElement($accessory);
        return $this;
    }

    /**
     * Get accessories
     * @return \Nitra\ProductBundle\Document\Product[] $accessories
     */
    public function getAccessories()
    {
        return $this->accessories;
    }

    /**
     * Set accessories
     * @param \Nitra\ProductBundle\Document\Product[] $accessories
     * @return self
     */
    public function setAccessories($accessories)
    {
        $this->accessories = $accessories;
        return $this;
    }
}